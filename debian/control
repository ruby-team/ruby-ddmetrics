Source: ruby-ddmetrics
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake,
               ruby-fuubar,
               ruby-rspec,
               ruby-rspec-its,
               ruby-timecop
Standards-Version: 4.2.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-ddmetrics.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-ddmetrics
Homepage: https://github.com/ddfreyne/ddmetrics
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: ruby-ddmetrics
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby:any | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Non-timeseries measurements for Ruby programs
 DDMetrics is a Ruby library for recording and analysing measurements in
 short-running Ruby processes.
 .
 Metrics are not recorded over time, and the time series are not available.
 Metrics data (particularly summary metrics) can accumulate in memory and cause
 memory pressure. This project is not suited for long-running processes, such as
 servers. The implementation is not thread-safe.
